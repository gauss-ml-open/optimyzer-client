# Copyright (c) 2020-2021, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer Client, which is released under the BSD 3-Clause License.


# type: ignore
# pylint: disable=missing-module-docstring

import math

import pytest

import optimyzer


def test_float_sampling():
    """
    Simple integration test for sampling from parameterspace.
    """
    p1_min = -1
    p1_max = 3
    p2_min = 1
    p2_max = 10

    # check the basics
    ps = optimyzer.parameterspace.ParameterSpace()
    ps.add(optimyzer.FloatParameter("p1", (p1_min, p1_max)))
    ps.add(optimyzer.FloatParameter("p2", (p2_min, p2_max)))

    for _ in range(100):
        sample = ps.sample()

        assert p1_min <= sample["p1"] <= p1_max
        assert p2_min <= sample["p2"] <= p2_max

    # check some corner cases
    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.FloatParameter("p1", (p1_max, p1_min)))

    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.FloatParameter("p1", [p1_min, p1_max]))


def test_integer_sampling():
    """
    Test whether sampling integers works.
    """
    p_min = -1
    p_max = 3

    # check the basics
    ps = optimyzer.parameterspace.ParameterSpace()
    ps.add(optimyzer.IntParameter("p1", (p_min, p_max)))

    for _ in range(100):
        sample = ps.sample()

        assert p_min <= sample["p1"] <= p_max

    # check some corner cases
    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.IntParameter("p1", (p_max, p_min)))

    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.IntParameter("p1", [p_min, p_max]))

    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.IntParameter("p1", [1.1, p_max]))

    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.IntParameter("p2", [p_min, 3.7]))


@pytest.mark.flaky
def test_integer_sampling_distribution():
    """
    Test whether sampled integers have the right distribution.
    """
    p_min = -1
    p_max = 3

    # check the basics
    ps = optimyzer.parameterspace.ParameterSpace()
    ps.add(optimyzer.IntParameter("p1", (p_min, p_max)))

    N = 1000
    samples = [ps.sample()["p1"] for _ in range(N)]

    for i in range(p_min, p_max + 1):
        percentage = 100 * samples.count(i) / N
        target_percentage = 100 * (1 / (p_max - p_min + 1))
        print(f"{i:2d}: {percentage} % (should be around {target_percentage} %)")
        assert abs(percentage - target_percentage) < 5


@pytest.mark.flaky
def test_logarithmic_sampling_distribution():
    """
    Test whether sampled integers have the right distribution on log scale.
    """
    p_min = 1.0
    p_max = math.exp(5.0)

    ps = optimyzer.parameterspace.ParameterSpace()
    ps.add(optimyzer.FloatParameter("p1", (p_min, p_max), logarithmic=True))
    ps.add(optimyzer.IntParameter("p2", (int(p_min), int(p_max)), logarithmic=True))

    N = 1000

    samples = [ps.sample()["p1"] for _ in range(N)]
    for i in range(int(math.log(p_min)), math.ceil(math.log(p_max))):
        log_samples = [math.log(s) for s in samples]
        samples_in_range = [i <= ls < i + 1 for ls in log_samples]
        percentage = 100 * samples_in_range.count(True) / N
        target_percentage = 100 * (1 / (math.log(p_max) - math.log(p_min)))
        print(
            f"(float) {i:2d}: {percentage:2.1f} % (should be around {target_percentage:2.1f} %, "
            f"range: {i:2d} <= x < {i+1:2d})"
        )
        assert abs(percentage - target_percentage) < 5

    samples = [ps.sample()["p2"] for _ in range(N)]
    for i in range(int(math.log(p_min)), math.ceil(math.log(p_max))):
        log_samples = [math.log(s) for s in samples]
        samples_in_range = [i <= ls < i + 1 for ls in log_samples]
        percentage = 100 * samples_in_range.count(True) / N
        target_percentage = 100 * (1 / (math.log(p_max) - math.log(p_min)))
        print(
            f"(int) {i:2d}: {percentage:2.1f} % (should be around {target_percentage:2.1f} %, "
            f"range: {i:2d} <= x < {i+1:2d})"
        )
        assert abs(percentage - target_percentage) < 5


def test_categorical_sampling():
    """
    Test whether sampling categorical parameters works.
    """
    categories = ["apple", "banana", "orange"]

    # check the basics
    ps = optimyzer.parameterspace.ParameterSpace()
    cp = optimyzer.CategoricalParameter("p1", categories)
    ps.add(cp)
    for _ in range(20):
        sample = ps.sample()
        assert sample["p1"] in categories

    # check some corner cases
    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.CategoricalParameter("p1", []))
    with pytest.raises(RuntimeError):
        ps = optimyzer.parameterspace.ParameterSpace()
        ps.add(optimyzer.CategoricalParameter("p1", "apple"))


def test_reserved_parameter_names():
    """
    Test what happens when trying to use metadata names for parameters.
    """
    ps = optimyzer.parameterspace.ParameterSpace()

    with pytest.raises(RuntimeError):
        ps.add(optimyzer.FloatParameter("workdir", (1, 2)))

    with pytest.raises(RuntimeError):
        ps.add(optimyzer.FloatParameter("id", (1, 2)))

    with pytest.raises(RuntimeError):
        ps.add(optimyzer.FloatParameter("value", (1, 2)))
