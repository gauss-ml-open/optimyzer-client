# Copyright (c) 2020-2021, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer Client, which is released under the BSD 3-Clause License.

# type: ignore
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access
import os
import tempfile
from operator import itemgetter

import pytest

import optimyzer
from optimyzer.filesystem import Instance


def test_write_read_config():
    """
    Check whether the configuration is written and read correctly
    """

    config = dict()
    config["par1"] = 1
    config["par2"] = 13

    with tempfile.TemporaryDirectory() as tempdir:
        optimyzer.filesystem._write_config(tempdir, config)
        with pytest.raises(RuntimeError):
            optimyzer.filesystem._write_config(tempdir, config)
        read_config = optimyzer.filesystem._read_config(tempdir)
        assert config == read_config


def test_write_read_value():
    """
    Check whether the value is written and read correctly
    """

    value = 2.718
    with tempfile.TemporaryDirectory() as tempdir:
        optimyzer.filesystem._write_value(tempdir, value)
        read_value = optimyzer.filesystem._read_value(tempdir)
        assert value == read_value


def test_get_instances():
    """
    Check whether instances are loaded correctly
    """
    with tempfile.TemporaryDirectory() as tempdir:
        # create one instance
        config = dict()
        config["par1"] = 1
        config["par2"] = 13
        value = 2.718
        metadatadir = os.path.join(tempdir, "instance1", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_config(metadatadir, config)
        optimyzer.filesystem._write_value(metadatadir, value)

        # create another instance
        config = dict()
        config["par3"] = 11
        config["par4"] = 131
        value = 3.141
        metadatadir = os.path.join(tempdir, "instance2", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_config(metadatadir, config)
        optimyzer.filesystem._write_value(metadatadir, value)

        # create a symlink to an instance
        optimyzer.filesystem._symlink(
            os.path.join(tempdir, "best_instance"), os.path.join(tempdir, "instance1")
        )

        instances = optimyzer.filesystem._get_all_instances(tempdir)
        instances.sort(key=itemgetter(1))

        print(repr(instances))

        instances_frozen = [
            Instance(id="instance1", value=2.718, config=dict(par1=1, par2=13)),
            Instance(id="instance2", value=3.141, config=dict(par3=11, par4=131)),
        ]

        assert instances == instances_frozen


def test_read_empty_directory():
    """
    When we try to read config or value from an empty directory, we should get a graceful error
    handling.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        # try to read a config without having created one
        with pytest.raises(FileNotFoundError):
            config = optimyzer.filesystem._read_config(tempdir)

    with tempfile.TemporaryDirectory() as tempdir:
        # try to read a value without having reported one
        with pytest.raises(FileNotFoundError):
            value = optimyzer.filesystem._read_value(tempdir)

    with tempfile.TemporaryDirectory() as tempdir:
        # Create instance metadata witouth a config
        config = dict()
        config["par1"] = 1
        config["par2"] = 13
        value = 2.718
        metadatadir = os.path.join(tempdir, "instance1", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_value(metadatadir, value)  # no config

        # Create instance metadata
        config = dict()
        config["par1"] = 11
        config["par2"] = 131
        value = 3.141
        metadatadir = os.path.join(tempdir, "instance2", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_config(metadatadir, config)
        optimyzer.filesystem._write_value(metadatadir, value)

        # Create instance metadata, but not a value
        config = dict()
        config["par1"] = 111
        config["par2"] = 1311
        value = 1.234
        metadatadir = os.path.join(tempdir, "instance3", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_config(metadatadir, config)  # no value

        # Create instance metadata
        config = dict()
        config["par1"] = 12
        config["par2"] = 17
        value = 9.876
        metadatadir = os.path.join(tempdir, "instance4", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_config(metadatadir, config)
        optimyzer.filesystem._write_value(metadatadir, value)

        # We expect to get back instance2 and instance4, which have both a config and a value
        instances = optimyzer.filesystem._get_all_instances(tempdir)
        instances.sort(key=itemgetter(1))

        instances_frozen = [
            Instance(id="instance2", value=3.141, config=dict(par1=11, par2=131)),
            Instance(id="instance4", value=9.876, config=dict(par1=12, par2=17)),
        ]

        assert instances == instances_frozen


def test_get_best_instance():
    """Integration test to get the best instance."""

    with tempfile.TemporaryDirectory() as tempdir:

        # trying to find the best instance without having any should raise an EmptyWorkdirError
        with pytest.raises(optimyzer.filesystem.EmptyWorkdirError):
            optimyzer.filesystem._get_best_instance(tempdir, minimize=False)

        # create one instance
        config = dict()
        config["par1"] = 1
        config["par2"] = 13
        value = 2.718
        metadatadir = os.path.join(tempdir, "instance1", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_config(metadatadir, config)
        optimyzer.filesystem._write_value(metadatadir, value)

        # create another instance
        config = dict()
        config["par3"] = 11
        config["par4"] = 131
        value = 3.141
        metadatadir = os.path.join(tempdir, "instance2", ".optimyzer")
        os.makedirs(metadatadir)
        optimyzer.filesystem._write_config(metadatadir, config)
        optimyzer.filesystem._write_value(metadatadir, value)

        # create a symlink to an instance
        optimyzer.filesystem._symlink(
            os.path.join(tempdir, "best_instance"), os.path.join(tempdir, "instance1")
        )

        optimyzer.filesystem._get_best_instance(tempdir, minimize=False)
