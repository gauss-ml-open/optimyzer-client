# Copyright (c) 2020-2021, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer Client, which is released under the BSD 3-Clause License.

# type: ignore
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access
import json
import os
import sys
import tempfile

import pytest

import optimyzer


def test_get_too_early():
    """
    See whether we're raising and exception if we didn't initialize with get_config() first.
    """
    with tempfile.TemporaryDirectory() as tempdir:
        with pytest.raises(RuntimeError):
            oy = optimyzer.Optimyzer(tempdir)
            oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
            oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
            oy.get_workdir()

        with pytest.raises(RuntimeError):
            oy = optimyzer.Optimyzer(tempdir)
            oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
            oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
            oy.get_config()

        with pytest.raises(RuntimeError):
            oy = optimyzer.Optimyzer(tempdir)
            oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
            oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
            oy.report_result(0.0)


def test_write_configuration():
    """
    See whether Optimyzer writes the configuration correctly to JSON.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
        oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
        oy.create_config()
        config = oy.get_config()
        instancepath = oy.get_workdir()

        with open(os.path.join(instancepath, ".optimyzer", "config.json"), "r") as fp:
            json_config = optimyzer.Configuration(json.load(fp), "", "")

        config.workdir = ""
        config.id = ""

        assert config == json_config


def test_write_value():
    """
    See whether Optimyzer writes the value correctly to JSON.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
        oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
        oy = optimyzer.Optimyzer(tempdir)
        oy.create_config()
        instancepath = oy.get_workdir()
        value = 3.14
        oy.report_result(value)

        with open(os.path.join(instancepath, ".optimyzer", "value.json"), "r") as fp:
            json_value = json.load(fp)["value"]
        assert value == json_value


def create_several_runs(workdir, minimize=False):
    """
    Create a couple of runs for testing.

    Parameters
    ----------
    workdir : str
        The base directory for Optimyzer.
    minimize : bool
        Whether or not we are minimizing, by default False

    Returns
    -------
    str
        The working directory of the known optimal instance.
    """

    # one high-value run
    oy = optimyzer.Optimyzer(workdir, minimize)
    oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
    oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
    oy.create_config()
    high_instance = oy.get_workdir()
    oy.report_result(111.0)

    # a couple of medium-high runs
    for _ in range(10):
        oy = optimyzer.Optimyzer(workdir, minimize)
        oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
        oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
        oy.create_config()
        oy.report_result(11.0)

    # a couple of medium-low runs
    for _ in range(10):
        oy = optimyzer.Optimyzer(workdir, minimize)
        oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
        oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
        oy.create_config()
        oy.report_result(1.0)

    # one low-value run
    oy = optimyzer.Optimyzer(workdir, minimize)
    oy.add_parameter(optimyzer.FloatParameter("par1", (0, 3)))
    oy.add_parameter(optimyzer.FloatParameter("par2", (-1, 0)))
    oy.create_config()
    low_instance = oy.get_workdir()
    oy.report_result(0.1)

    return low_instance, high_instance


def test_get_best_config():
    """
    See whether we can get the best config via create_config(optimal=True) and get_optimal_config
    alike.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        create_several_runs(tempdir)

        # get best run with the create_config interface
        oy = optimyzer.Optimyzer(tempdir)
        created_config = oy.create_config(optimal=True)
        workdir = oy.get_workdir()
        value = optimyzer.filesystem._read_value(os.path.join(workdir, ".optimyzer"))

        # get best run with the get_optimal_config interface
        config = optimyzer.get_optimal_config(tempdir)

        created_config.value = 111.0

        assert created_config == config
        assert created_config.to_dict() == config.to_dict()
        assert workdir == config.workdir
        assert value == config.value


def test_get_specific_config():
    """
    See whether we can get a specific config via instance ID.
    """

    with tempfile.TemporaryDirectory() as tempdir:

        with pytest.raises(optimyzer.filesystem.EmptyWorkdirError):
            optimyzer.get_instance_config("thisisnotarealconfig", tempdir)

        create_several_runs(tempdir)

        # create one run with different range, so that we recognize it
        parameter_range = (4, 5)
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(optimyzer.FloatParameter("par1", parameter_range))
        oy.add_parameter(optimyzer.FloatParameter("par2", parameter_range))
        config_created = oy.create_config()
        specific_id = config_created.id
        oy.report_result(1.0)
        config_created.value = 1.0

        # retrieve
        config_loaded = optimyzer.get_instance_config(specific_id, tempdir)

        assert config_created == config_loaded

        with pytest.raises(RuntimeError):
            optimyzer.get_instance_config("thisisnotarealconfig", tempdir)


def test_get_best_config_minimize():
    """
    See whether Optimyzer finds the best configuration.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        low_instance, _ = create_several_runs(tempdir, minimize=True)

        # get best run
        oy = optimyzer.Optimyzer(tempdir, minimize=True)
        oy.create_config(optimal=True)
        instance = oy.get_workdir()
        value = optimyzer.filesystem._read_value(os.path.join(instance, ".optimyzer"))

        assert instance == low_instance
        assert value == 0.1

        # also check the best_instance path
        best_instance = os.path.join(tempdir, "best_instance")
        assert instance == os.path.realpath(best_instance)


def test_get_best_config_maximize():
    """
    See whether Optimyzer finds the best configuration when maximizing.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        _, high_instance = create_several_runs(tempdir)

        # get best run
        oy = optimyzer.Optimyzer(tempdir)
        oy.create_config(optimal=True)
        instance = oy.get_workdir()
        value = optimyzer.filesystem._read_value(os.path.join(instance, ".optimyzer"))

        assert instance == high_instance
        assert value == 111.0

        # also check the best_instance path
        best_instance = os.path.join(tempdir, "best_instance")
        assert instance == os.path.realpath(best_instance)


def test_get_best_config_relative():
    """
    See whether Optimyzer finds the best configuration in a relative path setting.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        os.chdir(tempdir)

        _, high_instance = create_several_runs(".")

        # get best run
        oy = optimyzer.Optimyzer(".")
        oy.create_config(optimal=True)
        instance = oy.get_workdir()
        value = optimyzer.filesystem._read_value(os.path.join(instance, ".optimyzer"))

        assert instance == high_instance
        assert value == 111.0

        # also check the best_instance path
        best_instance = os.path.join(tempdir, "best_instance")
        assert instance == os.path.realpath(best_instance)


def test_get_best_config_before_running():
    """
    See whether Optimyzer throws an error if we try to get the optimal instance before running any.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        os.chdir(tempdir)
        oy = optimyzer.Optimyzer(".")

        with pytest.raises(optimyzer.filesystem.EmptyWorkdirError):
            # get best run
            oy = optimyzer.Optimyzer(".")
            oy.create_config(optimal=True)


@pytest.mark.flaky
def test_random_search_rosenbrock():
    """
    A simple benchmark: The Rosenbrock function.
    """

    def rosenbrock(x, y):
        a = 1.0 - x
        b = y - x * x
        return a * a + b * b * 100.0

    with tempfile.TemporaryDirectory() as tempdir:

        N = 200  # number of queries
        for _ in range(N):
            oy = optimyzer.Optimyzer(tempdir, minimize=True)
            oy.add_parameter(optimyzer.FloatParameter("x", (0, 3)))
            oy.add_parameter(optimyzer.FloatParameter("y", (0, 3)))
            config = oy.create_config()
            oy.report_result(rosenbrock(config.x, config.y))

        # get best run
        oy = optimyzer.Optimyzer(tempdir, minimize=True)
        config = oy.create_config(optimal=True)
        instance = config.id

        best_value = optimyzer.filesystem._read_value(os.path.join(tempdir, instance, ".optimyzer"))

        assert best_value < 1
        assert abs(1 - config.x) < 1
        assert abs(1 - config.y) < 1

        # test whether the best instance really is the best
        instances = optimyzer.filesystem._get_all_instances(tempdir)
        for instance_id, value, config in instances:
            if value <= best_value:
                assert instance_id == instance


def test_chdir_feature():
    """
    Test whether the current working directory is at the instance directory after creating the
    configuration.
    """
    try:
        current_wd = os.getcwd()
    except FileNotFoundError:
        # unning this with pytest, os.getcwd() fails. This is a workaround:
        current_wd = sys.path[0]

    with tempfile.TemporaryDirectory() as tempdir:
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(optimyzer.FloatParameter("x", (0, 3)))
        oy.add_parameter(optimyzer.FloatParameter("y", (0, 3)))
        oy = optimyzer.Optimyzer(tempdir)
        oy.create_config(chdir=True)

        assert os.getcwd() == oy.get_workdir()
        os.chdir(current_wd)


def test_overwriting_optimal_run():
    """
    Try to overwrite the optimal run and see if it throws a RuntimeWarning.
    """
    with tempfile.TemporaryDirectory() as tempdir:
        # sample once
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(optimyzer.FloatParameter("x", (0, 3)))
        oy.add_parameter(optimyzer.FloatParameter("y", (0, 3)))
        oy.create_config()
        oy.report_result(0.1)

        # sample twice
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(optimyzer.FloatParameter("x", (0, 3)))
        oy.add_parameter(optimyzer.FloatParameter("y", (0, 3)))
        oy.create_config()
        oy.report_result(1.1)

        # load optimal run
        oy = optimyzer.Optimyzer(tempdir)
        oy.create_config(optimal=True)
        with pytest.raises(RuntimeWarning):
            oy.report_result(3.0)


def test_adding_parameters_too_late():
    """
    Try to add a parameter after creating the config.
    """

    with tempfile.TemporaryDirectory() as tempdir:
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(optimyzer.FloatParameter("x", (0, 3)))
        config = oy.create_config()
        len(config.__dict__)
        with pytest.raises(RuntimeError):
            oy.add_parameter(optimyzer.FloatParameter("y", (0, 3)))


def test_class_as_categories():
    """Try to add classes as categorical parameter, create and retrieve
    configurations and see whether the classes are reconstructed correctly."""

    with tempfile.TemporaryDirectory() as tempdir:
        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(
            optimyzer.CategoricalParameter("c", [optimyzer.FloatParameter, optimyzer.IntParameter])
        )
        oy.add_parameter(optimyzer.IntParameter("i", (1, 8)))
        oy.add_parameter(optimyzer.FloatParameter("f", (1, 8)))
        oy.add_parameter(optimyzer.CategoricalParameter("s", ["apple", "banana"]))

        config = oy.create_config()

        assert config.c in [optimyzer.FloatParameter, optimyzer.IntParameter]


def test_find_classes_and_functions():
    """Unit-test for finding classes and functions from strings."""

    string_config = {
        "d": "<json.dump>",
        "c": "<optimyzer.Configuration>",
        "s": "<sum>",
        "f": "<float>",
        "i": "<int>",
        "find": "<optimyzer.get_optimal_config>",
        "ne": "<module.does.not.exist>",  # should remain untouched (doesn't exist)
        "ut": "json.dump",  # should remain untouched (no <>)
    }

    config = optimyzer.optimyzer.find_classes_and_functions(string_config)

    assert config["d"] == json.dump
    assert config["c"] == optimyzer.Configuration
    assert config["s"] == sum
    assert config["f"] == float
    assert config["i"] == int
    assert config["find"] == optimyzer.get_optimal_config
    assert config["ne"] == "<module.does.not.exist>"


def test_integration_list_best_configurations():
    """Just execute `list_best_configurations` to catch if it throws exceptions."""

    with tempfile.TemporaryDirectory() as tempdir:
        create_several_runs(tempdir)

        oy = optimyzer.Optimyzer(tempdir)
        oy.add_parameter(
            optimyzer.CategoricalParameter(
                "cat", [optimyzer.FloatParameter, optimyzer.IntParameter]
            )
        )
        oy.add_parameter(optimyzer.IntParameter("int", (1, 8)))
        oy.add_parameter(optimyzer.FloatParameter("float", (1, 8)))
        oy.add_parameter(optimyzer.CategoricalParameter("string", ["apple", "banana"]))

        oy.create_config()
        oy.report_result(200)

        optimyzer.list_best_configurations(tempdir)


def test_global_minimize_state():
    """Check whether the global minimization state works."""

    with tempfile.TemporaryDirectory() as tempdir:
        optimyzer.Optimyzer(tempdir, minimize=True)
        oy2 = optimyzer.Optimyzer(tempdir)

        assert oy2._minimize is True

        with pytest.raises(RuntimeError):
            optimyzer.Optimyzer(tempdir, minimize=False)

    with tempfile.TemporaryDirectory() as tempdir:
        optimyzer.Optimyzer(tempdir)
        oy2 = optimyzer.Optimyzer(tempdir)

        assert oy2._minimize is False

        with pytest.raises(RuntimeError):
            optimyzer.Optimyzer(tempdir, minimize=True)
