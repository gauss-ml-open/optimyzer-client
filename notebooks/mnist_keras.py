# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Tutorial: Integrate Optimyzer into a Simple Keras Workflow
#
# In this tutorial, you will learn how Optimyzer is integrated into Keras. This is a very basic
# tutorial, where we load the MNIST handwritten digit recognition dataset and classify it with a
# neural network. We use a three-layer network with dropout.
#
# Before we can start, you'll have to install Tensorflow, for example by running `conda install
# tensorflow`, if you're working with Conda.

# %%
# We're only importing Tensorflow and Optimyzer. Batteries are included!

import tensorflow as tf

import optimyzer

# %%
# The dataset is already included in Keras.
mnist = tf.keras.datasets.mnist

# A little bit of preprocessing
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# %% [markdown]
# We don't want to set the number of hidden units or the dropout probability ourselves. We just want
# to define the architeture and let Optimyzer take care of the rest.
#
# Let's initialize Optimyzer and define our parameters.

# %%
# Setting up Optimyzer
oy = optimyzer.Optimyzer("optimyzer_tmp")  # basedir is relative to the current working directory
oy.add_parameter(optimyzer.IntParameter("n_dense", (1, 128)))  # number of hidden units
oy.add_parameter(optimyzer.FloatParameter("p_dropout", (0, 1)))  # dropout probability


# %%
# Sampling a random configuration from the parameterspace
config = oy.create_config()  # the config then contains the parameters as attributes


# %%
# Building a simple model: one hidden layer, with dropout
model = tf.keras.models.Sequential(
    [
        tf.keras.layers.Flatten(input_shape=(28, 28)),  # (28, 28) is the size of the input images
        tf.keras.layers.Dense(config.n_dense),  # Optimyzer: Here we use the first parameter...
        tf.keras.layers.Dropout(config.p_dropout),  # ... and here the second
        tf.keras.layers.Dense(10),  # 10 is the number of classes (the digits 0-9 in MNIST)
    ]
)


# %%
# We're using an off-the-shelf loss function for classification
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)


# %%
# Here the training is happening. This may take a while...
model.compile(optimizer="adam", loss=loss_fn, metrics=["accuracy"])
model.fit(x_train, y_train, epochs=10)  # we're running ADAM for 10 epochs


# %%
# We can now evaluate the performance of our trained model
performance = model.evaluate(x_test, y_test, verbose=2)[1]
print(
    f"The performance for this run with n_dense={config.n_dense} and "
    f"p_dropout={config.p_dropout:.2f} was: {performance:2.2f}."
)


# %%
# This is the important step: We're telling Optimyzer how good this instance was.
oy.report_result(performance)

# %% [markdown]
# This was of course just the first run. You could go back to the top and re-run this notebook a
# couple of times. Or make a script and run it an a bash loop. Or you can use the loop below:

# %%
for i in range(10):
    config = oy.create_config()
    model = tf.keras.models.Sequential(
        [
            tf.keras.layers.Flatten(
                input_shape=(28, 28)
            ),  # (28, 28) is the size of the input images
            tf.keras.layers.Dense(config.n_dense),  # Optimyzer: Here we use the first parameter...
            tf.keras.layers.Dropout(config.p_dropout),  # ... and here the second
            tf.keras.layers.Dense(10),  # 10 is the number of classes (the digits 0-9 in MNIST)
        ]
    )
    model.compile(optimizer="adam", loss=loss_fn, metrics=["accuracy"])
    model.fit(x_train, y_train, epochs=10)  # we're running ADAM for 10 epochs
    performance = model.evaluate(x_test, y_test, verbose=2)[1]
    print(
        f"The performance for this run with n_dense={config.n_dense} and "
        f"p_dropout={config.p_dropout} was: {performance:2.2f}."
    )
    oy.report_result(performance)

# %% [markdown]
# After running this neural network training a couple of times, you can use Optimyzer to get the
# best parameter values that have been tried so far:

# %%
optimal_config = optimyzer.get_optimal_config("optimyzer_tmp")
print(
    f"The best run had the parameters n_dense={optimal_config.n_dense} and "
    f"p_dropout={optimal_config.p_dropout:.2f}, with a value of {optimal_config.value:.2f}."
)
