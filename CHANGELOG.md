# Changelog for the Optimyzer

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.2] - 2021-03-24 - Bugfix Release

### Fixed

- make it possible to first load the optimal config and then sample new ones

### Changed

- improved documentation of the parameterspace
- made properties out of some member variables

## [0.2.1] - 2021-03-10 - Bugfix Release

### Fixed

- create basedir automatically when trying to write the minimize flag

## [0.2.0] - 2021-02-19 - Feature Release

### Added

- CategoricalParameter class
- option to have classes and functions as categorical parameter
- deployability check in CI pipeline
- optimyzer.get_instance_config()
- optimyzer.list_best_configurations() to inspect the best runs
- global state for minimize, initialized with the first instance
- support for logarithmic distribution for IntParameter and FloatParameter

### Changed

- CD: switched to two-stage deployment, main(beta)->GitLab, release->PyPI
- more verbose twine deployment
- use differnt environment variable for PyPI token
- Copyright year
- remove special keys from the config when converted to dict
- add instance id to list output

## [0.1.2] - 2020-12-18 - Maintenance Release

### Added

- CI: black, pylint, flake8, mypy, pytest, pytest-cov
- CD: automatic deployment to PyPI

## [0.1.1] - 2020-12-01 - Maintenance Release

### Added

- CHANGELOG.md
- information about alpha status

### Changed

- repository location
- minor details in the readme

## [0.1.0] - 2020-11-24 - Initial Release

### Added

- Optimyzer's basic functionality, based on random search
